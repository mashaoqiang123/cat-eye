class Config(object):
    SECRET_KEY = 'ma5211314'
    SQLALCHEMY_DATABASE_URI = 'mysql://root:123456@localhost:3306/cateye'
    SQLALCHEMY_TRACK_MODIFICATIONS = True

class DevelopmentConfig(Config):
    DEBUG = True

class ProjectConfig(Config):
    pass

config_map = {
    'develop':DevelopmentConfig,
    'project':ProjectConfig
}