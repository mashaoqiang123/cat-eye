from . import db

# 年份与上映电影数量模型
class YearMovieCount(db.Model):
    __tablename__ = 'db_year_movie_count'
    id = db.Column(db.Integer,primary_key=True,autoincrement=True)
    year = db.Column(db.String(64),nullable=False)
    count = db.Column(db.Integer,nullable=False)

# 地区与上映电影数量前十模型
class AreaMovieCount(db.Model):
    __tablename__ = 'db_area_movie_count'
    id = db.Column(db.Integer,primary_key=True,autoincrement=True)
    area = db.Column(db.String(64),nullable=False)
    count = db.Column(db.Integer,nullable=False)

# 票房前十电影模型
class BookingMovieCount(db.Model):
    __tablename__ = 'db_booking_movie_count'
    id = db.Column(db.Integer,primary_key=True,autoincrement=True)
    name = db.Column(db.String(64), nullable=False)
    booking = db.Column(db.Float, nullable=False)

# 评论前十电影模型
class CommentMovieCount(db.Model):
    __tablename__ = 'db_comment_movie_count'
    id = db.Column(db.Integer,primary_key=True,autoincrement=True)
    name = db.Column(db.String(64),nullable=False)
    comment = db.Column(db.BigInteger,nullable=False)

# 票房前十电影模型
class ScoreMovieCount(db.Model):
    __tablename__ = 'db_score_movie_count'
    id = db.Column(db.Integer,primary_key=True,autoincrement=True)
    score = db.Column(db.String(64),nullable=False)
    count = db.Column(db.Integer,nullable=False)

# 类型与上映电影数量前十模型
class TypeMovieCount(db.Model):
    __tablename__ = 'db_type_movie_count'
    id = db.Column(db.Integer,primary_key=True,autoincrement=True)
    type = db.Column(db.String(64),nullable=False)
    count = db.Column(db.Integer,nullable=False)

