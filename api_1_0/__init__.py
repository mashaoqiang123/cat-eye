from flask import Flask
import pymysql
from flask_sqlalchemy import SQLAlchemy
pymysql.install_as_MySQLdb()
from config import  config_map
db = SQLAlchemy()

def create_app(mode = 'develop'):
    app = Flask(__name__)
    config = config_map[mode]
    # 加载配置类
    app.config.from_object(config)

    db.init_app(app)

    # 导入蓝图
    from . import views
    app.register_blueprint(views.blue,url_prefix='/show')


    return app