from api_1_0.views import blue
from api_1_0.model import db,AreaMovieCount,BookingMovieCount,CommentMovieCount,ScoreMovieCount,TypeMovieCount,YearMovieCount
from flask import render_template

# 年份与上映电影数量折线图
@blue.route('/drawLine')
def drawLine():
    year_count = YearMovieCount.query.all()
    year = [i.year for i in year_count]
    count = [i.count for i in year_count]

    return render_template('drawLine.html',**locals())

# 票房前十电影柱状图和评论前十电影模型柱状图
@blue.route('/drawBar')
def drawBar():
    movie_booking = BookingMovieCount.query.all()
    booking_name = [i.name for i in movie_booking]
    new_booking_name = []
    for i in booking_name:
        i = list(i)
        if len(i)>6:
            i.insert(len(i)//2,'\n')
        new_booking_name.append(''.join(i))
    booking = [i.booking for i in movie_booking]

    movie_comment = CommentMovieCount.query.all()
    comment_name = [i.name for i in movie_comment]
    new_comment_name = []
    for i in comment_name:
        i = list(i)
        if len(i) > 6:
            i.insert(len(i) // 2, '\n')
        new_comment_name.append(''.join(i))
    comment = [i.comment//10000 for i in movie_comment]

    return render_template('drawBar.html',**locals())

# 地区与上映电影数量前十模型占比图和类型与上映电影数量前十模型占比图
@blue.route('/drawPie')
def drawPie():
    area_movie_count = AreaMovieCount.query.all()
    area_count = [{'name':i.area,'value':i.count} for i in area_movie_count]

    type_movie_count = TypeMovieCount.query.all()
    type_count = [{'name':i.type,'value':i.count} for i in type_movie_count]

    return render_template('drawPie.html',**locals())

# 不同评分区间电影数量占比图
@blue.route('/drawSignalPie')
def drawSignalPie():
    score_movie_count = ScoreMovieCount.query.all()
    score_count = [{'name':i.score,'value':i.count} for i in score_movie_count]
    return render_template('drawSignalPie.html',**locals())