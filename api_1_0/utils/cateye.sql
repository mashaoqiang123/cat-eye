/*
 Navicat Premium Data Transfer

 Source Server         : my_mysql
 Source Server Type    : MySQL
 Source Server Version : 50729
 Source Host           : localhost:3306
 Source Schema         : cateye

 Target Server Type    : MySQL
 Target Server Version : 50729
 File Encoding         : 65001

 Date: 18/01/2021 11:29:47
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for alembic_version
-- ----------------------------
DROP TABLE IF EXISTS `alembic_version`;
CREATE TABLE `alembic_version`  (
  `version_num` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`version_num`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of alembic_version
-- ----------------------------
INSERT INTO `alembic_version` VALUES ('fbe0374d8a0a');

-- ----------------------------
-- Table structure for db_area_movie_count
-- ----------------------------
DROP TABLE IF EXISTS `db_area_movie_count`;
CREATE TABLE `db_area_movie_count`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `area` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `count` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 11 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of db_area_movie_count
-- ----------------------------
INSERT INTO `db_area_movie_count` VALUES (1, '中国大陆', 487);
INSERT INTO `db_area_movie_count` VALUES (2, '美国', 359);
INSERT INTO `db_area_movie_count` VALUES (3, '中国香港', 87);
INSERT INTO `db_area_movie_count` VALUES (4, '日本', 64);
INSERT INTO `db_area_movie_count` VALUES (5, '英国', 58);
INSERT INTO `db_area_movie_count` VALUES (6, '法国', 33);
INSERT INTO `db_area_movie_count` VALUES (7, '德国', 26);
INSERT INTO `db_area_movie_count` VALUES (8, '加拿大', 20);
INSERT INTO `db_area_movie_count` VALUES (9, '中国台湾', 19);
INSERT INTO `db_area_movie_count` VALUES (10, '印度', 17);

-- ----------------------------
-- Table structure for db_booking_movie_count
-- ----------------------------
DROP TABLE IF EXISTS `db_booking_movie_count`;
CREATE TABLE `db_booking_movie_count`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `booking` float NOT NULL,
  `name` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 11 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of db_booking_movie_count
-- ----------------------------
INSERT INTO `db_booking_movie_count` VALUES (1, 56.94, '战狼2');
INSERT INTO `db_booking_movie_count` VALUES (2, 50.35, '哪吒之魔童降世');
INSERT INTO `db_booking_movie_count` VALUES (3, 46.86, '流浪地球');
INSERT INTO `db_booking_movie_count` VALUES (4, 42.5, '复仇者联盟4：终局之战');
INSERT INTO `db_booking_movie_count` VALUES (5, 36.51, '红海行动');
INSERT INTO `db_booking_movie_count` VALUES (6, 33.98, '唐人街探案2');
INSERT INTO `db_booking_movie_count` VALUES (7, 33.91, '美人鱼');
INSERT INTO `db_booking_movie_count` VALUES (8, 31.7, '我和我的祖国');
INSERT INTO `db_booking_movie_count` VALUES (9, 31.09, '八佰');
INSERT INTO `db_booking_movie_count` VALUES (10, 31, '我不是药神');

-- ----------------------------
-- Table structure for db_comment_movie_count
-- ----------------------------
DROP TABLE IF EXISTS `db_comment_movie_count`;
CREATE TABLE `db_comment_movie_count`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `comment` bigint(20) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 21 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of db_comment_movie_count
-- ----------------------------
INSERT INTO `db_comment_movie_count` VALUES (11, '战狼2', 4888000);
INSERT INTO `db_comment_movie_count` VALUES (12, '哪吒之魔童降世', 3777000);
INSERT INTO `db_comment_movie_count` VALUES (13, '美人鱼', 3128000);
INSERT INTO `db_comment_movie_count` VALUES (14, '我不是药神', 2664000);
INSERT INTO `db_comment_movie_count` VALUES (15, '红海行动', 2649000);
INSERT INTO `db_comment_movie_count` VALUES (16, '流浪地球', 2531000);
INSERT INTO `db_comment_movie_count` VALUES (17, '唐人街探案2', 2451000);
INSERT INTO `db_comment_movie_count` VALUES (18, '速度与激情7', 2124000);
INSERT INTO `db_comment_movie_count` VALUES (19, '速度与激情8', 1860000);
INSERT INTO `db_comment_movie_count` VALUES (20, '复仇者联盟4：终局之战', 1811000);

-- ----------------------------
-- Table structure for db_score_movie_count
-- ----------------------------
DROP TABLE IF EXISTS `db_score_movie_count`;
CREATE TABLE `db_score_movie_count`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `score` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `count` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of db_score_movie_count
-- ----------------------------
INSERT INTO `db_score_movie_count` VALUES (1, '<7.0', 103);
INSERT INTO `db_score_movie_count` VALUES (2, '7.0-8.0', 236);
INSERT INTO `db_score_movie_count` VALUES (3, '>8.0', 663);

-- ----------------------------
-- Table structure for db_type_movie_count
-- ----------------------------
DROP TABLE IF EXISTS `db_type_movie_count`;
CREATE TABLE `db_type_movie_count`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `count` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 11 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of db_type_movie_count
-- ----------------------------
INSERT INTO `db_type_movie_count` VALUES (1, '剧情', 407);
INSERT INTO `db_type_movie_count` VALUES (2, '动作', 365);
INSERT INTO `db_type_movie_count` VALUES (3, '喜剧', 282);
INSERT INTO `db_type_movie_count` VALUES (4, '冒险', 277);
INSERT INTO `db_type_movie_count` VALUES (5, '爱情', 191);
INSERT INTO `db_type_movie_count` VALUES (6, '奇幻', 166);
INSERT INTO `db_type_movie_count` VALUES (7, '动画', 165);
INSERT INTO `db_type_movie_count` VALUES (8, '科幻', 144);
INSERT INTO `db_type_movie_count` VALUES (9, '犯罪', 132);
INSERT INTO `db_type_movie_count` VALUES (10, '惊悚', 95);

-- ----------------------------
-- Table structure for db_year_movie_count
-- ----------------------------
DROP TABLE IF EXISTS `db_year_movie_count`;
CREATE TABLE `db_year_movie_count`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `year` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `count` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 35 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of db_year_movie_count
-- ----------------------------
INSERT INTO `db_year_movie_count` VALUES (1, '1972', 1);
INSERT INTO `db_year_movie_count` VALUES (2, '1983', 1);
INSERT INTO `db_year_movie_count` VALUES (3, '1987', 1);
INSERT INTO `db_year_movie_count` VALUES (4, '1988', 2);
INSERT INTO `db_year_movie_count` VALUES (5, '1989', 1);
INSERT INTO `db_year_movie_count` VALUES (6, '1992', 2);
INSERT INTO `db_year_movie_count` VALUES (7, '1993', 1);
INSERT INTO `db_year_movie_count` VALUES (8, '1994', 4);
INSERT INTO `db_year_movie_count` VALUES (9, '1995', 3);
INSERT INTO `db_year_movie_count` VALUES (10, '1996', 1);
INSERT INTO `db_year_movie_count` VALUES (11, '1997', 4);
INSERT INTO `db_year_movie_count` VALUES (12, '1998', 7);
INSERT INTO `db_year_movie_count` VALUES (13, '1999', 3);
INSERT INTO `db_year_movie_count` VALUES (14, '2000', 5);
INSERT INTO `db_year_movie_count` VALUES (15, '2001', 3);
INSERT INTO `db_year_movie_count` VALUES (16, '2002', 11);
INSERT INTO `db_year_movie_count` VALUES (17, '2003', 14);
INSERT INTO `db_year_movie_count` VALUES (18, '2004', 10);
INSERT INTO `db_year_movie_count` VALUES (19, '2005', 12);
INSERT INTO `db_year_movie_count` VALUES (20, '2006', 17);
INSERT INTO `db_year_movie_count` VALUES (21, '2007', 15);
INSERT INTO `db_year_movie_count` VALUES (22, '2008', 20);
INSERT INTO `db_year_movie_count` VALUES (23, '2009', 30);
INSERT INTO `db_year_movie_count` VALUES (24, '2010', 31);
INSERT INTO `db_year_movie_count` VALUES (25, '2011', 30);
INSERT INTO `db_year_movie_count` VALUES (26, '2012', 33);
INSERT INTO `db_year_movie_count` VALUES (27, '2013', 48);
INSERT INTO `db_year_movie_count` VALUES (28, '2014', 33);
INSERT INTO `db_year_movie_count` VALUES (29, '2015', 50);
INSERT INTO `db_year_movie_count` VALUES (30, '2016', 106);
INSERT INTO `db_year_movie_count` VALUES (31, '2017', 123);
INSERT INTO `db_year_movie_count` VALUES (32, '2018', 124);
INSERT INTO `db_year_movie_count` VALUES (33, '2019', 173);
INSERT INTO `db_year_movie_count` VALUES (34, '2020', 83);

SET FOREIGN_KEY_CHECKS = 1;
